// SECTION I. Dependencies
const User = require("../models/User");
const Product = require("../models/Product");
const Order = require("../models/Order");
const bcrypt = require("bcrypt");
const auth = require("../auth");

// SECTION II. User Order Checkout
module.exports.checkOut = (req,res)=>{
    let total = 0;
    let length = req.body.products.length;
    let count = 1;
    if(req.user.isAdmin){
        return res.send({message: "Please use your personal account to check out."});
    }

    req.body.products.forEach(product =>
    {

        Product.findOne({_id:product.productId})
        .then(result => {       
            
            total = (result.price*product.quantity) + total;
            if(count === length){
                let newOrder = new Order({
                    totalAmount: total,
                    userId: req.user.id,
                    products: req.body.products
                })
                newOrder.save()
                .then(result => res.send({message: "Order Successful."}))
                .catch(error => res.send(error))
            } else {
                count++;    
            }
            
        })
        .catch(error => res.send(error));
        
    })

    // let newOrder = new Order({

    //  totalAmount: total,
    //  userId: req.user.id,
    //  products: req.body.products,

    // })

    // newOrder.save()
    // .then(result => res.send(result))
    // .catch(error => res.send(error))


}

// SECTION III. Retrieve Authenticated User Order
module.exports.viewLoginUserOrder = (req,res)=>{

    Order.find({userId:req.user.id})
    .then(result => {
        if (result === null){
            return res.send({message: "No Orders Found."})
        } else {
            return res.send(result)
        }
    })
    .catch(error => res.send(error))

}

// SECTION IV. Retrieve All Orders
module.exports.viewAllOrders = (req,res)=>{

    Order.find({})
    .then(result => res.send(result))
    .catch(error => res.send(error));


}

// SECTION V. Retrieve A Specific User Order/s
module.exports.viewUserOrder = (req,res)=>{

    Order.find({userId:req.params.userId},{_id:0,totalAmount:1,products:1})
    .then(result => {
        if (result === null){
            return res.send({message: "No Orders Found."});
        } else {
            return res.send(result);
        }
    })
    .catch(error => res.send(error));


}

// SECTION VI. Retrieve an Order
module.exports.viewOrder = (req,res) =>{

    Order.find({_id:req.params.orderId})
    .then(result => {
        if (result === null){
            return res.send({message: "No Orders Found."});
        } else {
            return res.send(result);
        }
    })
    .catch(error => res.send(error));
}

// SECTION VII. View Products Per Order
module.exports.viewProductsPerOrder = (req,res) =>{

    Order.find({_id:req.params.orderId},{_id:0,userId:1})
    .then(foundUser => {
        let result = foundUser.map(a => a.userId);
        if (result[0] !== req.user.id) {
            return res.send({message: "Wrong User"})
        } else {
            Order.find({_id:req.params.orderId},{products:1})
            .then(result => {
                if (result === null){
                    return res.send({message: "No Orders Found."});
                } else {
                    return res.send(result);
                }
            })
            .catch(error => res.send(error));           
        }
    })
    .catch(error => res.send(error));

}
