// SECTION I. Dependencies
const User = require("../models/User");
const Product = require("../models/Product");
const Order = require("../models/Order");
const bcrypt = require("bcrypt");
const auth = require("../auth");

// SECTION II. Registration
module.exports.registerUser = (req,res)=>{

    const hashedPw = bcrypt.hashSync(req.body.password,7);

    let newUser = new User({

        firstName: req.body.firstName,
        lastName: req.body.lastName,
        email: req.body.email,
        password: hashedPw,
        mobileNo: req.body.mobileNo

    })

    newUser.save()
    .then(result => res.send(result))
    .catch(error => res.send(error))

}

// SECTION III. User Login
module.exports.loginUser = (req,res)=>{

    User.findOne({email:req.body.email})
    .then(foundUser => {

        if (foundUser === null){

            return res.send({message: "No User Found."})

        } else {
            
            const isPasswordCorrect = bcrypt.compareSync(req.body.password, foundUser.password);

            if(isPasswordCorrect){
                return res.send({accessToken: auth.createAccessToken(foundUser)});
            } else {
                return res.send({message: "Incorrect Password"});
            }
        }
    })

}

// SECTION IV. Retrieve User Details
module.exports.viewUserDetails = (req,res) =>{

    console.log(req.user.id)

    User.find({_id:req.user.id})
    .then(result => {
        if (result === null){
            return res.send({message: "User not found."})
        } else {
            return res.send(result)
        }
    })
    .catch(error => res.send(error))

}

// SECTION V. Set User As Admin
module.exports.setAdmin = (req,res) => {

    let update = {
        isAdmin: true
    }

    User.findByIdAndUpdate(req.params.userId,update,{new:true})
    .then(result => res.send(result))
    .catch(error => res.send(error))

}
