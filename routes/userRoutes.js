// SECTION I. Dependencies
const express = require("express");
const router = express.Router();
const userControllers = require("../controllers/userControllers");
const auth = require("../auth");
const { verify,verifyAdmin } = auth;


// SECTION II. Registration Route
router.post('/',userControllers.registerUser);

// SECTION III. User Login/Authentication
router.post('/login',userControllers.loginUser);

// SECTION IV. View User Details
router.get('/userDetails',verify,userControllers.viewUserDetails);

// SECTION V. Set User as Admin
router.put('/setAdmin/:userId',verify,verifyAdmin,userControllers.setAdmin);

// SECTION ?. Router Export
module.exports = router;
