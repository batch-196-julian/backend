// SECTION I. Dependencies
const mongoose = require("mongoose");

// SECTION II. Schema
const productSchema = new mongoose.Schema({

    name: {
        type: String,
        required: [true, "Product name is required"]
    },
    description: {
        type: String,
        required: [true, "Product description is required"]
    },
    price: {
        type: Number,
        required: [true, "Product price is required"]
    },
    isActive: {
        type: Boolean,
        default: true
    },
    createdOn: {
        type: Date,
        default: new Date()
    }
});

// SECTION III. Model Export
module.exports = mongoose.model('Product', productSchema);
