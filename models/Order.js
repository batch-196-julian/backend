// SECTION I. Dependencies
const mongoose = require("mongoose");

// SECTION II. Schema
const orderSchema = new mongoose.Schema({

	totalAmount: {
		type: Number,
		required: [true, "Total amount is required"]
	},
	purchasedOn: {
		type: Date,
		default: new Date()
	},
	userId: {
		type: String,
		required: [true, "User is required"]
	},
	products: [
		{
			productId: {
				type: String,
				required: [true, "Product is required"]
			},
			quantity: {
				type: Number,
				required: [true, "Quantity is required"]
			}
		}
	]
});

// SECTION III. Model Export
module.exports = mongoose.model('Order', orderSchema);
