// SECTION I. Dependencies
const jwt = require("jsonwebtoken");
const secret = "ecommerceAPI"

// SECTION II. Create Token
module.exports.createAccessToken = (userDetails) => {

    const data = {
        id: userDetails.id,
        email: userDetails.email,
        isAdmin: userDetails.isAdmin
    }

    return jwt.sign(data,secret,{});
}

// SECTION III. Verify User
module.exports.verify = (req,res,next) => {
    
    let token = req.headers.authorization

    if(typeof token === "undefined"){
        return res.send({auth: "Failed. No Token."})
    } else {

        token = token.slice(7);
        jwt.verify(token,secret,function(err,decodedToken){

            if(err){
                return res.send({
                    auth: "Failed",
                    message: err.message
                })
            } else {
                req.user = decodedToken;
                next();
            }


        })
    }

}

// SECTION IV. Verify if user is Admin
module.exports.verifyAdmin = (req,res,next) => {

    if(req.user.isAdmin){
        next();
    } else{
        return res.send({
            auth: "Failed",
            message: "You are not authorized."
        })
    }
}
